# Proyecto Base Frontend


## Pasos de instalación
### 0) Instalar nvm

```bash
wget -qO- https://raw.githubusercontent.com/creationix/nvm/v0.31.2/install.sh | bash
source ~/.bashrc
```

### 0-1) Instalar Node

```bash
nvm install 6.9.1
nvm use 6.9.1
```

### 0-2) Instalar los ejecutables globales

```bash
npm install -g gulp cordova
```

### 1) Copiar el repositorio (No hace falta que sea un fork/clon)

```bash
https://gitlab.com/ciris/base-frontend
```


### 2) Instalar las dependencias del Proyecto

```bash
cd $proyecto
npm install
```

## Variables de entorno
 - NODE_ENV: para saber si se corre en modo "production" o en modo desarrollo
 - BACKEND: la url/ip del API de backend
 - ODOO_HOST: la url/ip del API de Odoo
 - ODOO_DB: el nombre de la base de datos a usar en Odoo
 - STOREPASS: el password del keystore para compilar android


## Correr el proyecto
### Desarrollo Web
```bash
gulp
```

### Herramientas de QA
```bash
gulp qa
gulp test
```

### Desarrollo Cordova
```bash
gulp cordova
```

Para correrlo en dispositivos
```bash
gulp cordova:run-ios
gulp cordova:run-android
```

## Cordova

### Construcción
La construcción "compila" todos los recursos javascript y luego genera los archivos de la plataforma
respectiva.

```bash
gulp cordova:build-ios
gulp cordova:build-android
```

Si se quiere para RELEASE
```bash
NODE_ENV=production gulp cordova:build-ios
NODE_ENV=production gulp cordova:build-android
```

### Compilación
La compilacón genera los ejecutables (apks). SIEMPRE debe usarse en modo production

```bash
NODE_ENV=production STOREPASS=miclavesecreta gulp cordova:compile-android
```

La compilación para ios está pendiente de investigar

# Documentación y Librerías
 * Reglex-grid    → http://leejordan.github.io/reflex/docs/
 * Angular        → https://angularjs.org/  |  Egghead: https://egghead.io/technologies/angularjs
 * Moment         → http://momentjs.com/docs
 * Lodash         → https://lodash.com/docs
 * Sass           → http://sass-lang.com/guide/
 * UI-Router      → https://github.com/angular-ui/ui-router/wiki
 * Satellizer     → https://github.com/sahat/satellizer
