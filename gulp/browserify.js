"use strict";

var gulp = require( "gulp" );
var watchify = require( "watchify" );
var browserify = require( "browserify" );
var source = require( "vinyl-source-stream" );
var buffer = require( "vinyl-buffer" );
var uglify = require( "gulp-uglify" );
var gulpif = require( "gulp-if" );
var sourcemaps = require( "gulp-sourcemaps" );
var _ = require( "lodash" );
var rutas = require( "./util/rutas.js" );
var notificar = require( "./util/notificar.js" );
var variables = require( "./util/variablesEntorno.js" );
var debug = require( "debug" )( "Gulp:browserify" );

var packageJson = require( "../package.json" );
var dependencias = Object.keys( packageJson && packageJson.dependencies || {} );
var manuales = [ "moment/locale/es" ];
var excepciones = [ "reflex-grid", "animate.css" ];
var dependenciasYManuales = _.union( dependencias, manuales );
var dependenciasCuradas = _.xor( dependenciasYManuales, excepciones );


module.exports = {
  vendor: browserifyVendor,
  app: browserifyApp,
  appWatch: watchifyApp
};

function browserifyVendor() {
  var b = browserify( {
    debug: !variables.produccion
  } );
  _.forEach( dependenciasCuradas, function( dep ) {
    debug( dep );
    b.require( dep );
  } );
  return b
    .bundle()
    .pipe( source( "vendor.js" ) )
    .pipe( buffer() )
    .pipe( gulpif( variables.produccion, sourcemaps.init( {loadMaps: true, largeFile: true} ) ) )
    .pipe( gulpif( variables.produccion, uglify() ) )
    .pipe( gulpif( variables.produccion, sourcemaps.write( "./" ) ) )
    .pipe( gulp.dest( rutas.dist ) )
    .on( "error", notificar( "Error de browserify - bundle" ) );
}

function browserifyApp() {
  var b = browserify( {
    entries: rutas.scripts.main,
    fullPaths: !variables.produccion,
    debug: !variables.produccion
  } );
  return compilar( b );
}

function watchifyApp() {
  var b = watchify( browserify( {
    entries: rutas.scripts.main,
    fullPaths: true,
    debug: true
  } ) );
  b.on( "update", function() {
    debug( "RECARGANDO APP.JS con debug:true" );
    return compilar( b );
  } );
  b.on( "error", notificar( "Error de watchify - app" ) );
  return compilar( b );
}

function compilar( b ) {
  return b
    .external( dependenciasCuradas )
    .bundle()
    .pipe( source( "app.js" ) )
    .pipe( buffer() )
    .pipe( sourcemaps.init( {loadMaps: true, largeFile: true} ) )
    .pipe( sourcemaps.write( "./" ) )
    .pipe( gulp.dest( rutas.dist ) )
    .on( "error", notificar( "Error de browserify - app" ) );
}
