"use strict";

var rutas = require( "./util/rutas.js" );
var historyApiFallback = require( "connect-history-api-fallback" );
var browserSync = require( "./util/browserSyncInstance.js" );

module.exports = servidor;

var archivos = [
  rutas.dist + "/*.css",
  rutas.dist + "/*.js",
  rutas.dist + "/*.map",
  rutas.dist + "/*.html",
  rutas.dist + "/recursos/**/*"
];

var opciones = {
    server: {
      baseDir: rutas.dist,
      index: "index.html"
    },
    ui: {
      port: 3010,
      weinre: {
        port: 3011
      }
    },
    https: false,
    middleware: [ historyApiFallback() ],
    files: archivos,
    online: false,
    notify: true,
    reloadDelay: 500,
    reloadDebounce: 2000,
    injectChanges: true,
    host: "0.0.0.0",
    logLevel: "warn",
    logPrefix: "browser-sync",
    ghostMode: false
  };

function servidor( cb ) {
  browserSync().init( opciones );
  return cb();
}
