"use strict";

var gulp = require( "gulp" );
var ngConstant = require( "gulp-ng-constant" );
var rename = require( "gulp-rename" );
var rutas = require( "./util/rutas.js" );
var notificar = require( "./util/notificar.js" );
var variables = require( "./util/variablesEntorno.js" );

module.exports = constantes;

function constantes( ) {
  return ngConstant( {
      name: "Constantes",
      constants: {
        "backend": variables.backend,
        "daemon": variables.daemon,
        "produccion": variables.produccion,
        "odooHost": variables.odooHost,
        "odooDB": variables.odooDB
      },
      stream: true,
      wrap: false
    } )
    .pipe( rename( "constantes.js" ) )
    .pipe( gulp.dest( rutas.dist ) )
    .on( "error", notificar( "Error en el archivo de constantes" ) );
}
