"use strict";
var variables = require( "../util/variablesEntorno.js" );
var execSync = require( "child_process" ).execSync;

module.exports = compilar;

function compilar( cb ) {
  if ( variables.storepass ) {
    execSync( "rm -rf production_ready.apk" );
    execSync( "cp platforms/android/build/outputs/apk/android-release-unsigned.apk temporal.apk" );
    execSync( "jarsigner -verbose -sigalg SHA1withRSA -digestalg SHA1 -keystore ciris.keystore temporal.apk ciris -storepass cirisCiris.93" );
    execSync( "zipalign -v 4 temporal.apk production_ready.apk" );
    execSync( "rm -rf temporal.apk" );
    return cb();
  } else {
    console.error( new Error( "Olvidó proporcionar el PASSWORD del keystore. La variable de entorno se llama STOREPASS" ) );
    return process.exit( 1 );
  }

}
