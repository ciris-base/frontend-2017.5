"use strict";

var cordova = require( "cordova-lib" ).cordova;
var opciones = require( "./options.js" );

module.exports = {
  android: build( "android" ),
  ios: build( "ios" )
};

function build ( platform ) {
  return function ( callback ) {
    return cordova.build( {
      "platforms": [ platform ],
      "options": opciones
      }, callback );
  };
}
