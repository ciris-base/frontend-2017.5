"use strict";

var gulp = require( "gulp" );
var build = require( "./build.js" );
var run = require( "./run.js" );
var recursos = require( "./recursos.js" );

gulp.task( "cordova:html-inicial", require( "../html-inicial.js" ).cordova );

gulp.task( "cordova:watch-android", recursos.android );
gulp.task( "cordova:watch-ios", recursos.ios );
gulp.task( "cordova:watch-browser", recursos.browser );
gulp.task( "cordova:watch", [ "cordova:watch-android", "cordova:watch-ios", "cordova:watch-browser" ] );

gulp.task( "cordova:prepare", require( "./prepare.js" ) );
gulp.task( "cordova:build-android", [ "cleanbuild", "cordova:html-inicial", "cordova:prepare" ], build.android );
gulp.task( "cordova:build-ios", [ "cleanbuild", "cordova:html-inicial", "cordova:prepare" ], build.ios );

gulp.task( "cordova:compile-android", [ "cordova:build-android" ], require( "./androidCompile.js" ) );

gulp.task( "cordova:run-android", [ "cleanbuild", "cordova:html-inicial", "cordova:prepare", "watch" ], run.android );
gulp.task( "cordova:run-ios", [ "cleanbuild", "cordova:html-inicial", "cordova:prepare", "watch" ], run.ios );
gulp.task( "cordova:run-browser", [ "cleanbuild", "cordova:html-inicial", "cordova:prepare", "watch" ], run.browser );
gulp.task( "cordova:serve", [ "cleanbuild", "cordova:html-inicial", "cordova:prepare", "watch" ], require( "./serve.js" ) );
gulp.task( "cordova", [ "cordova:run-browser" ] );
