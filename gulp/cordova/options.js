"use strict";
var variables = require( "../util/variablesEntorno.js" );

module.exports = [ variables.produccion ? "--release" : "--debug", "--gradleArg=--no-daemon", "--port 3000" ];
