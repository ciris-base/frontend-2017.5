"use strict";

var cordova = require( "cordova-lib" ).cordova;
var opciones = require( "./options.js" );

module.exports = prepare;

function prepare ( callback ) {
  cordova.prepare( {options:opciones}, callback );
}
