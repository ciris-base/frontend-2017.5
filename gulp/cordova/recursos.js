"use strict";
var gulp = require( "gulp" );
var rutas = require( "../util/rutas.js" );
var notificar = require( "../util/notificar.js" );

module.exports = {
  android: recursos( "android" ),
  ios: recursos( "ios" ),
  browser: recursos( "browser" )
};

function recursos( platform ) {
  return function( ) {
    return gulp.src( rutas.cordova.recursos, {base: "./www"} )
      .pipe( gulp.dest( rutas.cordova[platform] ) )
      .on( "error", notificar( "Error copiando los recursos" ) );
  };
}
