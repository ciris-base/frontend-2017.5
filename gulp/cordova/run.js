"use strict";

var cordova = require( "cordova-lib" ).cordova;
var opciones = require( "./options.js" );

module.exports = {
  android: run( "android" ),
  ios: run( "ios" ),
  browser: run( "browser" )
};

function run ( platform ) {
  return function ( callback ) {
    return cordova.run( {
      "platforms": [ platform ],
      "options": opciones
      }, callback );
  };
}
