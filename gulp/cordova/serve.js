"use strict";

var cordova = require( "cordova-lib" ).cordova;
var opciones = require( "./options.js" );

module.exports = serve;

function serve ( callback ) {
  cordova.raw.serve( {options:opciones}, callback );
}
