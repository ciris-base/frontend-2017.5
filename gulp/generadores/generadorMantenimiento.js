"use strict";

var fs = require( "fs" );
var mkdirp = require( "mkdirp" );
var Log = require( "log" );
var log = new Log( "info" );

module.exports = generador;

function tests( nombre, modulo, archivo, ars ) {
  mkdirp( ruta( modulo, "./src/app/modulo/models/test/unit" ), function() {
    fs.writeFile( ruta( modulo, "./src/app/modulo/models/test/unit/" + archivo + ".js" ), ars.modeloTest );
  } );

  /*mkdirp( ruta( modulo, "./test/app/modulo/ctrls" ), function() {
    fs.writeFile( ruta( modulo, "test/app/modulo/ctrls/form" +
     nombre + "Ctrl.js" ), ars.formCtrlTest );
     fs.writeFile( ruta( modulo, "test/app/modulo/ctrls/listado" +
      nombre + "Ctrl.js" ), ars.listadoCtrlTest );
  } );*/
}

function normal( nombre, modulo, archivo, ars ) {
  mkdirp( ruta( modulo, "./src/app/modulo/models" ), function() {
    fs.writeFile( ruta( modulo, "src/app/modulo/models/" + archivo + ".js" ), ars.modelo );
  } );
  mkdirp( ruta( modulo, "./src/app/modulo/ctrls/rest" ), function() {
    fs.writeFile( ruta( modulo, "src/app/modulo/ctrls/rest/" +
     archivo + "API.js" ), ars.rest );
  } );
  fs.writeFile( ruta( modulo, "src/app/modulo/views/lista" + nombre + ".html" ), ars.lista );
  fs.writeFile( ruta( modulo, "src/app/modulo/views/form" + nombre + ".html" ), ars.uno );
  fs.writeFile( ruta( modulo, "src/app/modulo/ctrls/listado" +
   nombre + "Ctrl.js" ), ars.listaCtrl );
  fs.writeFile( ruta( modulo, "src/app/modulo/ctrls/form" +
   nombre + "Ctrl.js" ), ars.formCtrl );

  fs.writeFile( ruta( modulo, "src/app/modulo/rutas.js" ), agregarRutas( modulo, ars.rutas ) );
  fs.writeFile( ruta( modulo, "src/app/modulo/index.js" ),
   agregarAngular( modulo, ars.angular ) );
}

function generador( cb ) {
  try {
    var nombre = capitalizar( require( "yargs" ).argv.nombre );
    var modulo = require( "yargs" ).argv.modulo.toLowerCase();
    var archivo = nombre.toLowerCase();
    var ars = archivos( nombre, archivo, modulo );

    normal( nombre, modulo, archivo, ars );
    tests( nombre, modulo, archivo, ars );

    log.info( "Mantenimiento generado." );
    cb();
  } catch ( es ) {
    log.error( es );
  }
}

function agregarRutas( modulo, rutas ) {
  var rutasActuales = fs.readFileSync( ruta( modulo, "./src/app/modulo/rutas.js" ), "utf-8" );
  var temp = rutasActuales.substring( 0, rutasActuales.lastIndexOf( "}" ) );
  return temp + rutas + "\n}";
}

function agregarAngular( modulo, angular ) {
  var indexActual = fs.readFileSync( ruta( modulo, "./src/app/modulo/index.js" ), "utf-8" );
  var temp = indexActual.substring( 0, indexActual.lastIndexOf( "module.exports = mod.name;" ) );
  return temp + angular + "\nmodule.exports = mod.name;";
}

function archivos( nombre, archivo, modulo ) {
  var modelo = fs.readFileSync( "gulp/generadores/mantenimiento/modelo.js", "utf-8" );
  var rest = fs.readFileSync( "gulp/generadores/mantenimiento/rest.js", "utf-8" );
  var lista = fs.readFileSync( "gulp/generadores/mantenimiento/lista.html", "utf-8" );
  var uno = fs.readFileSync( "gulp/generadores/mantenimiento/uno.html", "utf-8" );
  var listaCtrl = fs.readFileSync( "gulp/generadores/mantenimiento/listadoCtrl.js", "utf-8" );
  var formCtrl = fs.readFileSync( "gulp/generadores/mantenimiento/formCtrl.js", "utf-8" );
  var rutas = fs.readFileSync( "gulp/generadores/mantenimiento/rutas.js", "utf-8" );
  var angular = fs.readFileSync( "gulp/generadores/mantenimiento/angular.js", "utf-8" );
  var modeloTest = fs.readFileSync( "gulp/generadores/mantenimiento/modeloTest.js", "utf-8" );

  /*Var formCtrlTest = fs.readFileSync( "gulp/generadores/mantenimiento/formCtrlTest.js", "utf-8" );
  var listadoCtrlTest =
    fs.readFileSync( "gulp/generadores/mantenimiento/listadoCtrlTest.js", "utf-8" );*/

  return {
    modelo: modelo.replace( /NOMBRE/g, nombre ).replace( /ARCHIVO/g, archivo ),
    rest: rest.replace( /NOMBRE/g, nombre ).replace( /ARCHIVO/g, archivo ),
    lista: lista.replace( /NOMBRE/g, nombre ).replace( /ARCHIVO/g, archivo ),
    uno: uno.replace( /NOMBRE/g, nombre ).replace( /ARCHIVO/g, archivo ),
    listaCtrl: listaCtrl.replace( /NOMBRE/g, nombre ).replace( /ARCHIVO/g, archivo ),
    formCtrl: formCtrl.replace( /NOMBRE/g, nombre ).replace( /ARCHIVO/g, archivo ),
    rutas: rutas.replace( /NOMBRE/g, nombre ).replace( /ARCHIVO/g, archivo )
      .replace( /MODULO/g, modulo ),
    angular: angular.replace( /NOMBRE/g, nombre ).replace( /ARCHIVO/g, archivo )
      .replace( /MODULO/g, modulo ),
    modeloTest: modeloTest.replace( /NOMBRE/g, nombre ).replace( /ARCHIVO/g, archivo )

    /*FormCtrlTest: formCtrlTest.replace( /NOMBRE/g, nombre ).replace( /ARCHIVO/g, archivo ),
    listadoCtrlTest: listadoCtrlTest.replace( /NOMBRE/g, nombre ).replace( /ARCHIVO/g, archivo )*/
  };
}

function ruta( modulo, url ) {
  return url.replace( "modulo", modulo );
}

function capitalizar( string ) {
  var lower = string.toLowerCase();
  return lower.charAt( 0 ).toUpperCase() + lower.slice( 1 );
}
