"use strict";

var _ = require( "lodash" );

module.exports = NOMBRE;

var modelo = {
  hacerAlgo: hacerAlgo
};

function NOMBRE( json ) {
  var temp = Object.create( modelo );
  var obj = _.assign( temp, json );
  return obj;
}

function hacerAlgo() {
  return "Algo";
}
