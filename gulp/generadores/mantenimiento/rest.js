"use strict";

module.exports = NOMBREAPI;
var _ = require( "lodash" );

var ARCHIVO = require( "../../models/ARCHIVO.js" );

function NOMBREAPI( backend, $http ) {
  NOMBREAPI.obtener = obtener;
  NOMBREAPI.listar = listar;
  NOMBREAPI.guardar = guardar;
  NOMBREAPI.eliminar = eliminar;
  return NOMBREAPI;

  function obtener( id ) {
    if ( id ) {
      return $http.get( backend + "/api/ARCHIVO/" + id ).then( function( resp ) {
        return ARCHIVO( resp.data );
      } );
    } else {
      return ARCHIVO( {} );
    }
  }

  function listar( pagina, cantidad ) {
    var params = {
      params: {
        pagina: pagina || 0,
        cantidad: cantidad || 10
      }
    };
    return $http.get( backend + "/api/ARCHIVO/", params ).then( ok, err );
  }

  function ok( resp ) {
    if ( resp.data.docs ) {
      resp.data.docs = _.map( resp.data.docs, function( elem ) {
        return ARCHIVO( elem );
      } );
    }
    return resp.data;
  }

  function err( ) {
    return {docs: [], contador: 0};
  }

  function eliminar( id ) {
    return $http.delete( backend + "/api/ARCHIVO/" + id ).then( function( resp ) {
      return resp.data;
    } );
  }

  function guardar( obj ) {
    if ( obj._id ) {
      return editar( obj );
    } else {
      return crear( obj );
    }
  }

  function crear( obj ) {
    return $http.post( backend + "/api/ARCHIVO/", obj ).then( function( resp ) {
      return ARCHIVO( resp.data );
    } );
  }

  function editar( obj ) {
    return $http.put( backend + "/api/ARCHIVO/" + obj._id, obj ).then( function( resp ) {
      return ARCHIVO( resp.data );
    } );
  }
}
