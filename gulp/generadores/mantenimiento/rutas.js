$stateProvider.state( "index.MODULO.ARCHIVO-lista", {
  templateUrl: "MODULO/views/listaNOMBRE.html",
  url: "/ARCHIVO?pagina&cantidad",
  controller: "ListadoNOMBRECtrl",
  controllerAs: "vm",
  resolve: {
    listado: listarNOMBRE
  },
  data: {
    titulo: "Listado de NOMBRE",
    icono: "fa-list"
  }
} );

$stateProvider.state( "index.MODULO.ARCHIVO-uno", {
  templateUrl: "MODULO/views/formNOMBRE.html",
  url: "/ARCHIVO/:id?editar",
  controller: "FormNOMBRECtrl",
  controllerAs: "vm",
  resolve: {
    ARCHIVO: obtenerNOMBRE
  },
  data: {
    titulo: "Form de NOMBRE",
    icono: "fa-file-o",
    oculto: true
  }
} );

function listarNOMBRE( NOMBREAPI, $stateParams ) {
  return NOMBREAPI.listar( $stateParams.pagina, $stateParams.cantidad );
}

function obtenerNOMBRE( NOMBREAPI, $stateParams ) {
  return NOMBREAPI.obtener( $stateParams.id, $stateParams.editar );
}
