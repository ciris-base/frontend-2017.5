"use strict";

var fs = require( "fs" );
var execSync = require( "child_process" ).execSync;
var readlineSync = require( "readline-sync" );
var _ = require( "lodash" );

module.exports = githook;
var question = "¿Qué tipo de cambio se hizo? [Mayor, Menor, Parche] (Parche)\n";
function githook( cb ) {
  var tipo = readlineSync.question( question );
  var paquete = readPackage();
  paquete.version = bump( paquete.version, tipo );
  writeConfig( bumpearConfig( readConfig(), paquete.version ) );
  writePackage( paquete );
  execSync( "git add package.json" );
  execSync( "git add config.xml" );
  return cb();
}

function readPackage() {
  return JSON.parse( fs.readFileSync( "package.json", "utf-8" ) );
}

function readConfig() {
  return fs.readFileSync( "config.xml", "utf-8" );
}

function writePackage( json ) {
  return fs.writeFileSync( "package.json", JSON.stringify( json, null, "  " ) );
}

function writeConfig( config ) {
  return fs.writeFileSync( "config.xml", config );
}

function versionConfig( config ) {
  var i = config.lastIndexOf( "version" ) + 9;
  return config.substring( i, i + 5 );
}

function androidVersion( config ) {
  var i = config.lastIndexOf( "android-versionCode" ) + 21;
  return config.substring( i, i + 6 );
}

function bumpearConfig( config, nuevaVersion ) {
  var version = versionConfig( config );
  var versionAndroid = androidVersion( config );
  var neo = config.replace( version, nuevaVersion ).replace( versionAndroid, parsearVersionAndroid( nuevaVersion ) );
  return neo;
}

function bump( version, tipo ) {
  var vers = _.map( version.split( "." ), function( ele ) {
    return Number( ele );
  } );
  if ( /ma/i.test( tipo ) ) {
    return ( vers[0] + 1 ) + ".0.0";
  }
  if ( /me/i.test( tipo ) ) {
    return vers[0] + "." + ( vers[1] + 1 ) +  ".0";
  }
  return vers[0] + "." + vers[1] +  "." + ( vers[2] + 1 );
}

function parsearVersionAndroid( version ) {
  var vers = _.map( version.split( "." ), function( ele ) {
    if ( ele.length === 2 ) {
      return ele;
    } else {
      return "0" + ele;
    }
  } );
  return vers[0] + vers[1] + vers[2];
}
