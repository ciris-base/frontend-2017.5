"use strict";
var gulp = require( "gulp" );
var rename = require( "gulp-rename" );
var rutas = require( "./util/rutas.js" );
var handlebars = require( "gulp-compile-handlebars" );
var notificar = require( "./util/notificar.js" );

module.exports = {
  web: htmlInicial( "web" ),
  cordova: htmlInicial( "cordova" )
};

function htmlInicial( platform ) {
  return function() {
    return gulp.src( rutas.htmlInicial[platform], {
        base: "./src/app"
      } )
      .pipe( handlebars( {version: require( "../package.json" ).version} ) )
      .pipe( rename( "index.html" ) )
      .pipe( gulp.dest( rutas.dist ) )
      .on( "error", notificar( "Error copiando los recursos" ) );
  };
}
