"use strict";

var gulp = require( "gulp" );
var browserify = require( "./browserify.js" );
var linterSass = require( "./lint/sass.js" );
var linterHtml = require( "./lint/html.js" );
var linterJs = require( "./lint/es.js" );
var tape = require( "./tape.js" );
var sass = require( "./sass.js" );
require( "./generadores/tareas.js" );
require( "./cordova" );

gulp.task( "clean", require( "./limpiar.js" ) );
gulp.task( "loc", require( "./loc.js" ) );

gulp.task( "lint:sass", linterSass.nofail );
gulp.task( "lint:js", linterJs.nofail );
gulp.task( "lint:html", linterHtml.nofail );
gulp.task( "lint", [ "lint:js", "lint:html", "lint:sass" ] );
gulp.task( "coverage", tape.coverage );
gulp.task( "test", tape.test );
gulp.task( "test-coverage", [ "coverage" ], tape.test );
gulp.task( "nightwatch", require( "./nightwatch.js" ) );
gulp.task( "pre-commit", [ "lint", "test-coverage" ], require( "./githooks.js" ) );


gulp.task( "build:recursos", require( "./recursos.js" ) );
gulp.task( "build:constantes", require( "./constantes.js" ) );
gulp.task( "build:html", require( "./plantillas.js" ) );
gulp.task( "build:sass", sass.main );
gulp.task( "build:sass-printer", sass.printer );
gulp.task( "build:app", browserify.app );
gulp.task( "build:vendor", browserify.vendor );
gulp.task( "build:js", [ "build:app", "build:vendor" ] );
gulp.task( "build", [
  "lint",
  "test-coverage",
  "build:js",
  "build:sass",
  "build:sass-printer",
  "build:html",
  "build:constantes",
  "build:recursos",
  "loc"
] );
gulp.task( "cleanbuild", [ "clean", "build" ] );

gulp.task( "web:html-inicial", require( "./html-inicial.js" ).web );
gulp.task( "web:build", [ "web:html-inicial", "build" ] );


gulp.task( "watch:js", browserify.appWatch );
gulp.task( "watch", [ "watch:js" ], require( "./util/watchers.js" ) );

gulp.task( "serve:dev", [ "clean", "cleanbuild", "web:build", "watch" ], require( "./browsersync.js" ) );
gulp.task( "default", [ "serve:dev" ] );
