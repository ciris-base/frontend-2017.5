"use strict";

var gulp = require( "gulp" );
var eslint = require( "gulp-eslint" );
var rutas = require( "../util/rutas.js" );
var notificar = require( "../util/notificar.js" );
var _ = require( "lodash" );

module.exports = {
  nofail: nofail,
  nocatch: lint
};

function lint( ) {
  return gulp.src( _.union( rutas.scripts.todo, rutas.tests.unit ) )
    .pipe( eslint() )
    .pipe( eslint.formatEach( ) )
    .pipe( eslint.failAfterError() );
}

function nofail( ) {
  return lint()
    .on( "error", function( err ) {
      notificar( "Error de ESLint" )( err );
      this.emit( "end" );
    } );
}
