"use strict";

var gulp = require( "gulp" );
var htmlLint = require( "gulp-html-lint" );
var rutas = require( "../util/rutas.js" );
var notificar = require( "../util/notificar.js" );

module.exports = {
  nofail: nofail,
  nocatch: lint
};

function lint( ) {
  return gulp.src( rutas.plantillas )
    .pipe( htmlLint() )
    .pipe( htmlLint.formatEach() )
    .pipe( htmlLint.failAfterError() );
}

function nofail( ) {
  return lint()
    .on( "error", function( err ) {
      notificar( "Error de HTMLint" )( err );
      this.emit( "end" );
    } );
}
