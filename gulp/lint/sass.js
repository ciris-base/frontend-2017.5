"use strict";

var gulp = require( "gulp" );
var sassLint = require( "gulp-sass-lint" );
var rutas = require( "../util/rutas.js" );
var notificar = require( "../util/notificar.js" );

module.exports = {
  nofail: nofail,
  nocatch: lint
};

function lint( ) {
  return gulp.src( rutas.sass.todo )
    .pipe( sassLint() )
    .pipe( sassLint.format() )
    .pipe( sassLint.failOnError() );
}

function nofail( ) {
  return lint()
    .on( "error", function( err ) {
      notificar( "Error de SassLint" )( err );
      this.emit( "end" );
    } );
}
