"use strict";

var gulp = require( "gulp" );
var sloc = require( "gulp-sloc" );
var rutas = require( "./util/rutas.js" );

module.exports = loc;

function loc() {
  return gulp.src( rutas.scripts.todo )
    .pipe( sloc() );
}
