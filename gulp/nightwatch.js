"use strict";

var gulp = require( "gulp" );
var nightwatch = require( "gulp-nightwatch" );
module.exports = coverage;

var opciones = {
  configFile: "nightwatch.json"
};

function coverage() {
  return gulp.src( "" )
    .pipe( nightwatch( opciones ) );
}
