"use strict";

var gulp = require( "gulp" );
var minifyHTML = require( "gulp-minify-html" );
var ngTemplate = require( "gulp-angular-templatecache" );
var rutas = require( "./util/rutas.js" );
var variables = require( "./util/variablesEntorno.js" );
var notificar = require( "./util/notificar.js" );

var ngTemplateOpts = {
  module: require( "../package.json" ).name
};

module.exports = plantillas;

function plantillas( terminar ) {
  var minifyOpts = {
    empty: !variables.produccion,
    spare: !variables.produccion,
    quotes: true,
    comments: !variables.produccion
  };
  return gulp.src( rutas.plantillas )
    .on( "error", function() {
      return terminar();
    } )
    .pipe( minifyHTML( minifyOpts ) )
    .pipe( ngTemplate( "plantillas.js", ngTemplateOpts ) )
    .pipe( gulp.dest( rutas.dist ) )
    .on( "error", notificar( "Error de Plantillas" ) );
}
