"use strict";
var gulp = require( "gulp" );
var rutas = require( "./util/rutas.js" );
var notificar = require( "./util/notificar.js" );

module.exports = recursos;

function recursos() {
  return gulp.src( rutas.recursos, {
      base: "./src"
    } )
    .pipe( gulp.dest( rutas.dist ) )
    .on( "error", notificar( "Error copiando los recursos" ) );
}
