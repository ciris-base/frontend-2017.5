"use strict";
var notify = require( "gulp-notify" );

module.exports = notificar;

function notificar( mensaje ) {
  return notify.onError( {
    title:    mensaje,
    message:  "<%= error.message %>",
    sound:    "Bottle"
  } );
}
