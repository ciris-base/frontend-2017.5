"use strict";

var rutas = {
  "dist": "www",
  "scripts": {
    "main": "src/app/index.js",
    "todo": [ "src/app/*.js", "src/app/**/*.js" ],
    "istanbul": [ "src/app/*.js", "src/app/**/*.js", "!src/app/**/*/test/unit/**/*.js" ]
  },
  "tests": {
    "unit": "src/app/**/*/test/unit/**/*.js"
  },
  "sass": {
    "main": "src/sass/main.scss",
    "printer": "src/sass/printer.scss",
    "todo": [ "src/sass/*.scss", "src/sass/**/*.scss", "src/app/**/*.scss" ]
  },
  "recursos": [ "src/recursos", "src/recursos/*", "src/recursos/**/*" ],
  "htmlInicial": {
    "web": "src/app/index-web.html",
    "cordova": "src/app/index-cordova.html"
  },
  "plantillas": [ "src/app/**/*.html", "!src/app/index.html" ],
  "cordova": {
    "recursos": [ "www", "www/*", "www/**/*" ],
    "browser":"platforms/browser/www",
    "android":"platforms/android/www",
    "ios":"platforms/ios/www"
  }
};

module.exports = rutas;
