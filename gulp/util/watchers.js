"use strict";

var gulp = require( "gulp" );
var _ = require( "lodash" );
var rutas = require( "./rutas.js" );

module.exports = watch;

function watch() {
  gulp.watch( rutas.sass.todo, [ "build:sass", "build:sass-printer" ] );
  gulp.watch( rutas.plantillas, [ "build:html" ] );
  gulp.watch( rutas.recursos, [ "build:recursos" ] );
  gulp.watch( rutas.htmlInicial.web, [ "web:html-inicial" ] );
  gulp.watch( rutas.htmlInicial.cordova, [ "cordova:html-inicial" ] );
  gulp.watch( rutas.cordova.recursos, [ "cordova:watch", "cordova:prepare" ] );
}
