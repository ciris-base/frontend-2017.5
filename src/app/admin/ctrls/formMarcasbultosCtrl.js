"use strict";

module.exports = FormMarcasbultosCtrl;

function FormMarcasbultosCtrl( marcasbultos, MarcasbultosAPI, $state, $stateParams, toastr ) {
  var vm = this;
  vm.marcasbultos = marcasbultos;
  vm.marcasbultos.editando = ( $stateParams.editar === "true" );
  vm.guardar = guardar;
  vm.editar = editar;
  vm.eliminar = eliminar;
  vm.listaPresentaciones = [ "48x1", "48x1 Premium Pride", "10x1", "Jumbo", "10Kg", "24x2", "Granel" ];

  function guardar( formValido ) {
    if ( formValido ) {
      if ( vm.marcasbultos.capacidadMax > vm.marcasbultos.capacidadMin ) {
        MarcasbultosAPI.guardar( vm.marcasbultos ).then( function( resp ) {
          reload( resp._id, false );
        } );
      } else {
        toastr.error( "La capacidad máxima no puede ser menor que la capacidad mínima", "Error" );
      }
    }
  }

  function editar( valor ) {
    reload( vm.marcasbultos._id, valor );
  }

  function eliminar() {
    MarcasbultosAPI.eliminar( vm.marcasbultos ).then( function() {
      $state.go( "^.marcasbultos-lista" );
    } );
  }

  function reload( id, editando ) {
    $state.go( $state.current, {id: id, editar: editando}, {reload: true} );
  }
}
