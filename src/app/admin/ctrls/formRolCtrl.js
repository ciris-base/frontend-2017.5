"use strict";

module.exports = FormRolCtrl;

function FormRolCtrl( rol, RolAPI, $state, $stateParams ) {
  var vm = this;
  vm.integridad = require( "../models/integridad.js" );
  vm.modulo = "rol";
  vm.rol = rol;
  vm.rol.editando = ( $stateParams.editar === "true" );
  vm.guardar = guardar;
  vm.editar = editar;
  vm.eliminar = eliminar;

  function guardar( formValido ) {
    if ( formValido ) {
      RolAPI.guardar( vm.rol ).then( function( resp ) {
        reload( resp._id, false );
      } );
    }
  }

  function editar( valor ) {
    reload( vm.rol._id, valor );
  }

  function eliminar() {
    RolAPI.eliminar( vm.rol ).then( function() {
      $state.go( "^.rol-lista" );
    } );
  }

  function reload( id, editando ) {
    $state.go( $state.current, {id: id, editar: editando}, {reload: true} );
  }
}
