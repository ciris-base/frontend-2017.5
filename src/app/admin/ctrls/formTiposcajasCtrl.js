"use strict";

module.exports = FormTiposcajasCtrl;

function FormTiposcajasCtrl( tiposcajas, TiposcajasAPI, $state, $stateParams ) {
  var vm = this;
  vm.tiposcajas = tiposcajas;
  vm.tiposcajas.editando = ( $stateParams.editar === "true" );
  vm.guardar = guardar;
  vm.editar = editar;
  vm.eliminar = eliminar;

  function guardar( formValido ) {
    if ( formValido ) {
      TiposcajasAPI.guardar( vm.tiposcajas ).then( function( resp ) {
        reload( resp._id, false );
      } );
    }
  }

  function editar( valor ) {
    reload( vm.tiposcajas._id, valor );
  }

  function eliminar() {
    TiposcajasAPI.eliminar( vm.tiposcajas ).then( function() {
      $state.go( "^.tiposcajas-lista" );
    } );
  }

  function reload( id, editando ) {
    $state.go( $state.current, {id: id, editar: editando}, {reload: true} );
  }
}
