"use strict";

module.exports = FormUsuarioCtrl;

function FormUsuarioCtrl( usuario, UsuarioAPI, $state, $stateParams ) {
  var vm = this;
  vm.integridad = require( "../models/integridad.js" );
  vm.modulo = "usuario";
  vm.usuario = usuario;
  vm.usuario.editando = ( $stateParams.editar === "true" );
  vm.guardar = guardar;
  vm.editar = editar;
  vm.eliminar = eliminar;

  function guardar( formValido ) {
    if ( formValido ) {
      UsuarioAPI.guardar( vm.usuario ).then( function( resp ) {
        reload( resp._id, false );
      } );
    }
  }

  function editar( valor ) {
    reload( vm.usuario._id, valor );
  }

  function eliminar() {
    UsuarioAPI.eliminar( vm.usuario ).then( function() {
      $state.go( "^.usuario-lista" );
    } );
  }

  function reload( id, editando ) {
    $state.go( $state.current, {id: id, editar: editando}, {reload: true} );
  }
}
