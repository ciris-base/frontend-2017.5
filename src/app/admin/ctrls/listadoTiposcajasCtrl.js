"use strict";

var _ = require( "lodash" );

module.exports = ListadoTiposcajasCtrl;

function ListadoTiposcajasCtrl( listado, TiposcajasAPI, $state, $stateParams ) {
  var vm = this;
  vm.pagina = parseInt( $stateParams.pagina || 0 ) + 1;
  vm.cantidad = parseInt( $stateParams.cantidad || 10 );
  vm.listado = listado;
  vm.eliminar = eliminar;
  vm.actualizarPagina = actualizarPagina;

  function eliminar( tiposcajas ) {
    if ( confirm( "¿Está seguro que desea eliminar el Tiposcajas?" ) ) {
      TiposcajasAPI.eliminar( tiposcajas._id ).then( function() {
        vm.listado.docs = _.reject( vm.listado.docs, function( elem ) {
          return elem._id === tiposcajas._id;
        } );
        vm.listado.contador -= 1;
      } );
    }
  }

  function actualizarPagina( pagina ) {
    $state.go( $state.current, {pagina: pagina, cantidad: vm.cantidad}, {reload: false} );
  }
}
