"use strict";

module.exports = MarcasbultosAPI;
var _ = require( "lodash" );

var marcasbultos = require( "../../models/marcasbultos.js" );

function MarcasbultosAPI( backend, $http ) {
  MarcasbultosAPI.obtener = obtener;
  MarcasbultosAPI.listar = listar;
  MarcasbultosAPI.guardar = guardar;
  MarcasbultosAPI.eliminar = eliminar;
  return MarcasbultosAPI;

  function obtener( id ) {
    if ( id ) {
      return $http.get( backend + "/api/marcasbulto/" + id ).then( function( resp ) {
        return marcasbultos( resp.data );
      } );
    } else {
      return marcasbultos( {} );
    }
  }

  function listar() {
    return $http.get( backend + "/api/marcasbulto/" ).then( ok, err );
  }

  function ok( resp ) {
    if ( resp.data.docs ) {
      resp.data.docs = _.map( resp.data.docs, function( elem ) {
        return marcasbultos( elem );
      } );
    }
    return resp.data;
  }

  function err( ) {
    return {docs: [], contador: 0};
  }

  function eliminar( id ) {
    return $http.delete( backend + "/api/marcasbulto/" + id ).then( function( resp ) {
      return resp.data;
    }, function( resp ) {
      return resp;
    } );
  }

  function guardar( obj ) {
    if ( obj._id ) {
      return editar( obj );
    } else {
      return crear( obj );
    }
  }

  function crear( obj ) {
    return $http.post( backend + "/api/marcasbulto/", obj ).then( function( resp ) {
      return marcasbultos( resp.data );
    }, function( resp ) {
      return resp;
    } );
  }

  function editar( obj ) {
    return $http.put( backend + "/api/marcasbulto/" + obj._id, obj ).then( function( resp ) {
      return marcasbultos( resp.data );
    }, function( resp ) {
      return resp;
    } );
  }
}
