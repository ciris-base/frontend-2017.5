"use strict";

module.exports = UsuarioAPI;
var _ = require( "lodash" );

var usuario = require( "../../models/usuario.js" );

function UsuarioAPI( backend, $http ) {
  UsuarioAPI.obtener = obtener;
  UsuarioAPI.listar = listar;
  UsuarioAPI.guardar = guardar;
  UsuarioAPI.eliminar = eliminar;
  return UsuarioAPI;

  function obtener( id ) {
    if ( id ) {
      return $http.get( backend + "/api/usuario/" + id ).then( function( resp ) {
        return usuario( resp.data );
      } );
    } else {
      return usuario( {} );
    }
  }

  function listar( pagina, cantidad ) {
    var params = {
      params: {
        pagina: pagina || 0,
        cantidad: cantidad || 10
      }
    };
    return $http.get( backend + "/api/usuario/", params ).then( ok, err );
  }

  function ok( resp ) {
    if ( resp.data.docs ) {
      resp.data.docs = _.map( resp.data.docs, function( elem ) {
        return usuario( elem );
      } );
    }
    return resp.data;
  }

  function err( ) {
    return {docs: [], contador: 0};
  }

  function eliminar( id ) {
    return $http.delete( backend + "/api/usuario/" + id ).then( function( resp ) {
      return resp.data;
    }, function( resp ) {
      return resp;
    } );
  }

  function guardar( obj ) {
    if ( obj._id ) {
      return editar( obj );
    } else {
      return crear( obj );
    }
  }

  function crear( obj ) {
    return $http.post( backend + "/api/usuario/", obj ).then( function( resp ) {
      return usuario( resp.data );
    }, function( resp ) {
      return resp;
    } );
  }

  function editar( obj ) {
    return $http.put( backend + "/api/usuario/" + obj._id, obj ).then( function( resp ) {
      return usuario( resp.data );
    }, function( resp ) {
      return resp;
    } );
  }
}
