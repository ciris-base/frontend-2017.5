"use strict";

var angular = require( "angular" );
var packageJson = require( "../../../package.json" );

var mod = angular.module( packageJson.name + ".Admin", [] );
mod.config( require( "./rutas" ) );

mod.controller( "ListadoRolCtrl", require( "./ctrls/listadoRolCtrl.js" ) );
mod.controller( "FormRolCtrl", require( "./ctrls/formRolCtrl.js" ) );
mod.factory( "RolAPI", require( "./ctrls/rest/rolAPI.js" ) );

mod.controller( "ListadoUsuarioCtrl", require( "./ctrls/listadoUsuarioCtrl.js" ) );
mod.controller( "FormUsuarioCtrl", require( "./ctrls/formUsuarioCtrl.js" ) );

mod.factory( "UsuarioAPI", require( "./ctrls/rest/usuarioAPI.js" ) );

mod.controller( "ListadoMarcasbultosCtrl", require( "./ctrls/listadoMarcasbultosCtrl.js" ) );
mod.controller( "FormMarcasbultosCtrl", require( "./ctrls/formMarcasbultosCtrl.js" ) );

mod.factory( "MarcasbultosAPI", require( "./ctrls/rest/marcasbultosAPI.js" ) );

mod.controller( "ListadoTiposcajasCtrl", require( "./ctrls/listadoTiposcajasCtrl.js" ) );
mod.controller( "FormTiposcajasCtrl", require( "./ctrls/formTiposcajasCtrl.js" ) );

mod.factory( "TiposcajasAPI", require( "./ctrls/rest/tiposcajasAPI.js" ) );

module.exports = mod.name;