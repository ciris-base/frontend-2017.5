"use strict";

var _ = require( "lodash" );

module.exports = Marcasbultos;

var modelo = {
  hacerAlgo: hacerAlgo
};

function Marcasbultos( json ) {
  var temp = Object.create( modelo );
  var obj = _.assign( temp, json );
  return obj;
}

function hacerAlgo() {
  return "Algo";
}
