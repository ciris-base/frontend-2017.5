"use strict";

var _ = require( "lodash" );

module.exports = Rol;

var modelo = {
  hacerAlgo: hacerAlgo
};

function Rol( json ) {
  var temp = Object.create( modelo );
  var obj = _.assign( temp, json );
  return obj;
}

function hacerAlgo() {
  return "Algo";
}
