"use strict";

var test = require( "tape" );
var integridad = require( "../../integridad.js" );

test( "PERMISOS: Activar 'ver' al activar cualquier otro permiso", activarVer );
test( "PERMISOS: NO activar 'ver' al desactivar cualquier otro permiso", noActivarVer );
test( "PERMISOS: Dejar activado 'ver' si algun otro permiso esta activo", noCambiarVer );
test( "PERMISOS: Poder desactivar 'ver' si no hay ningun otro permiso activo", cambiarVer );

function activarVer( assert ) {
  var permisos = {
    ver: false,
    editar: true,
    crear: false,
    eliminar: false
  };
  integridad.setearVer( permisos, "editar" );
  assert.deepEqual( permisos.ver, true );
  assert.end();
}

function noActivarVer( assert ) {
  var permisos = {
    ver: false,
    editar: false,
    crear: false,
    eliminar: false
  };
  integridad.setearVer( permisos, "editar" );
  assert.deepEqual( permisos.ver, false );
  assert.end();
}

function noCambiarVer( assert ) {
  var permisos = {
    ver: true,
    editar: true,
    crear: false,
    eliminar: false
  };
  integridad.validarVer( permisos, "editar" );
  assert.deepEqual( permisos.ver, true );
  assert.end();
}

function cambiarVer( assert ) {
  var permisos = {
    ver: false,
    editar: false,
    crear: false,
    eliminar: false
  };
  integridad.validarVer( permisos, "editar" );
  assert.deepEqual( permisos.ver, false );
  assert.end();
}
