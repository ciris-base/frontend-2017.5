"use strict";

var test = require( "tape" );
var modelo = require( "../../rol.js" );

test( "Hacer algo", hacerAlgo );

function hacerAlgo( assert ) {
  var alguien = modelo( {} );
  var algo = alguien.hacerAlgo();
  assert.deepEqual( algo, "Algo" );
  assert.end();
}
