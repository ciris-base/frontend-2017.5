"use strict";

var _ = require( "lodash" );

module.exports = Usuario;

var modelo = {
  aplicarPermisos: aplicarPermisos,
  hacerAlgo: hacerAlgo
};

function Usuario( json ) {
  var temp = Object.create( modelo );
  var obj = _.assign( temp, json );
  return obj;
}

function aplicarPermisos( permisos ) {
  Object.assign( this.permisos, permisos );
}

function hacerAlgo() {
  return "Algo";
}
