"use strict";

module.exports = rutas;

function rutas( $stateProvider ) {

  $stateProvider.state( "index.admin", {
    templateUrl: "admin/views/inicio.html",
    url: "/admin",
    data: {
      titulo: "Administración",
      icono: "fa-cog",
      oculto: function( Guardian ) {
        return Guardian.bloquear( "admin" );
      },
      bloqueado: function( Guardian ) {
        return Guardian.bloquear( "admin" );
      }
    }
  } );

  $stateProvider.state( "index.admin.rol-lista", {
    templateUrl: "admin/views/listaRol.html",
    url: "/rol?pagina&cantidad",
    controller: "ListadoRolCtrl",
    controllerAs: "vm",
    resolve: {
      listado: listarRol
    },
    data: {
      titulo: "Roles",
      icono: "fa-list",
      oculto: function( Guardian ) {
        return Guardian.bloquear( "admin.roles" );
      },
      bloqueado: function( Guardian ) {
        return Guardian.bloquear( "admin.roles" );
      }
    }
  } );

  $stateProvider.state( "index.admin.rol-uno", {
    templateUrl: "admin/views/formRol.html",
    url: "/rol/:id?editar",
    controller: "FormRolCtrl",
    controllerAs: "vm",
    resolve: {
      rol: obtenerRol
    },
    data: {
      titulo: "Formulario de Rol",
      icono: "fa-file-o",
      oculto: true,
      bloqueado: function( Guardian ) {
        return Guardian.bloquear( "admin.roles" );
      }
    }
  } );

  $stateProvider.state( "index.admin.usuario-lista", {
    templateUrl: "admin/views/listaUsuario.html",
    url: "/usuario?pagina&cantidad",
    controller: "ListadoUsuarioCtrl",
    controllerAs: "vm",
    resolve: {
      listado: listarUsuario
    },
    data: {
      titulo: "Usuarios",
      icono: "fa-list",
      oculto: function( Guardian ) {
        return Guardian.bloquear( "admin.usuarios" );
      },
      bloqueado: function( Guardian ) {
        return Guardian.bloquear( "admin.usuarios" );
      }
    }
  } );

  $stateProvider.state( "index.admin.usuario-uno", {
    templateUrl: "admin/views/formUsuario.html",
    url: "/usuario/:id?editar",
    controller: "FormUsuarioCtrl",
    controllerAs: "vm",
    resolve: {
      usuario: obtenerUsuario
    },
    data: {
      titulo: "Formulario de Usuario",
      icono: "fa-file-o",
      oculto: true,
      bloqueado: function( Guardian ) {
        return Guardian.bloquear( "admin.usuarios" );
      }
    }
  } );

  function listarRol( RolAPI, $stateParams ) {
    return RolAPI.listar( $stateParams.pagina, $stateParams.cantidad );
  }

  function obtenerRol( RolAPI, $stateParams ) {
    return RolAPI.obtener( $stateParams.id, $stateParams.editar );
  }

  function listarUsuario( UsuarioAPI, $stateParams ) {
    return UsuarioAPI.listar( $stateParams.pagina, $stateParams.cantidad );
  }

  function obtenerUsuario( UsuarioAPI, $stateParams ) {
    return UsuarioAPI.obtener( $stateParams.id, $stateParams.editar );
  }
}
