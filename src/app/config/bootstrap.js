"use strict";

var angular = require( "angular" );
module.exports = bootstrap;

function bootstrap( modulo ) {
  angular.element( document ).ready( function() {
    var yaInstanciado = angular.element( document ).injector();
    if ( !yaInstanciado ) {
      if ( window.cordova ) {
        document.addEventListener( "deviceready", function () {
          angular.bootstrap( document, [ modulo ] );
        }, false );
      } else {
        angular.bootstrap( document, [ modulo ] );
      }
    }
  } );
}
