"use strict";

module.exports = configurarSatellizer;

function configurarSatellizer( $authProvider, backend, prefix ) {
  $authProvider.storageType = "sessionStorage";
  $authProvider.loginUrl = backend + "/api/login/odoo";
  $authProvider.baseUrl = backend;
  $authProvider.tokenPrefix = prefix;
  $authProvider.oauth1( {
    name: "odoo",
    url: "/api/login/odoo",
    redirectUri: window.location.origin || window.location.protocol +
     "//" + window.location.host + "/main"
  } );
}
