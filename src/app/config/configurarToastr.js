"use strict";

var angular = require( "angular" );

module.exports = configurarToastr;

function configurarToastr( toastrConfig ) {
  angular.extend( toastrConfig, {
    autoDismiss: true,
    containerId: "toastrs",
    maxOpened: 0,
    newestOnTop: true,
    positionClass: "alertas",
    preventDuplicates: false,
    preventOpenDuplicates: false,
    target: "body",
    allowHtml: true,
    closeButton: true,
    closeHtml: "<i class='fa fa-fw fa-times alertas__alerta__cerrar'></i>",
    extendedTimeOut: 25000,
    iconClasses: {
      error: "alertas__alerta--error",
      info: "alertas__alerta--info",
      success: "alertas__alerta--success",
      warning: "alertas__alerta--warning"
    },
    messageClass: "alertas__alerta__mensaje",
    onHidden: null,
    onShown: null,
    onTap: null,
    progressBar: false,
    tapToDismiss: true,
    timeOut: 10000,
    titleClass: "alertas__alerta__titulo",
    toastClass: "alertas__alerta"
  } );
}
