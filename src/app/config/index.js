"use strict";

module.exports = config;
var proyecto = require( "../../../package.json" ).name;
var prefix = "ciris-" + proyecto;

function config( $locationProvider, $compileProvider, toastrConfig, cfpLoadingBarProvider,
$authProvider, backend, produccion ) {
  html5Mode( !window.cordova );
  $compileProvider.debugInfoEnabled( produccion );
  require( "./configurarToastr.js" )( toastrConfig );
  require( "./configurarLoadingBar.js" )( cfpLoadingBarProvider );
  require( "./configurarSatellizer.js" )( $authProvider, backend, prefix );

  function html5Mode( mode ) {
    if ( window.history && window.history.pushState ) {
      $locationProvider.html5Mode( {
        enabled: mode,
        requireBase: false
      } );
    }
  }

}
