"use strict";

module.exports = run;

function run( $rootScope, $auth, $state, toastr, $http, $window, Guardian ) {
  var stateChange = require( "./stateChange.js" );
  var stateChangeStart = stateChange( $rootScope, $auth, toastr, $state, $http, $window, Guardian );
  $rootScope.$on( "$stateChangeStart", stateChangeStart );
  $rootScope.$on( "$stateChangeError", stateChangeError );

  function stateChangeError( event, toState, toParams, fromState, fromParams, error ) {
    event.preventDefault();
    if ( error.autenticado === false ) {
      $state.go( "403" );
    } else {
      $state.go( "404" );
    }
  }
}
