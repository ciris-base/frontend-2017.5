"use strict";

module.exports = stateChange;

function stateChange( $rootScope, $auth, toastr, $state, $http, $window, Guardian ) {
  return function( event, toState ) {
    if ( $auth.isAuthenticated() ) {
      Guardian.setPermisos( $auth );
      $rootScope.$broadcast( "cerrar-menu" );
      prevenirPantallaDeLogin( event, toState );
      headerOdoo();
      deslogueoEnOtroLugar( event );
      validarPermisos( event, toState );
    } else {
      if ( $window.localStorage.usuario ) {
        logueoPorRecuerdo( event );
      } else {
        sinLogin( event, toState );
      }
    }
  };

  function validarPermisos( event, toState ) {
    if ( typeof toState.data.bloqueado === "function" ) {
      var bloquear = toState.data.bloqueado( Guardian );
      if ( bloquear ) {
        event.preventDefault();
        $state.go( "403" );
      }
    }
  }

  //Está logueado y quiere ir a login
  function prevenirPantallaDeLogin( event, toState ) {
    if ( toState.name === "login" ) {
      inicio( event );
    }
  }

  //Está logueado y se setean los valores de cabeceras default
  function headerOdoo() {
    if ( $window.sessionStorage.usuario ) {
      $http.defaults.headers.common.odooSession = $window.sessionStorage.usuario.sesionOdoo;
    }
  }

  function deslogueoEnOtroLugar( event ) {
    if ( !$window.localStorage.usuario && !$window.sessionStorage.usuario ) {
      event.preventDefault();
      $auth.logout();
      delete $window.sessionStorage.usuario;
      toastr.warning( "Ha finalizado la sesión desde otro lugar", "Advertencia" );
      $state.go( "login" );
    }
  }

  function logueoPorRecuerdo( event ) {
    $window.sessionStorage.usuario = $window.localStorage.usuario;
    var usuario = JSON.parse( $window.sessionStorage.usuario );
    $auth.setToken( usuario.token );
    Guardian.setPermisos( $auth );
    inicio( event );
    toastr.info( "Bienvenido de vuelta " + usuario.nombre, "Información" );
  }

  function sinLogin( event, toState ) {
    if ( !toState.free ) {
      event.preventDefault();
      toastr.warning( "Necesita estar autenticado para ver estos recursos", "Advertencia" );
      $state.go( "login" );
    }
  }

  function inicio( event ) {
    event.preventDefault();
    $state.go( "index.main" );
  }
}
