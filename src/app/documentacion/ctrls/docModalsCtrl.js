"use strict";

module.exports = docModalsCtrl;

function docModalsCtrl( ModalService ) {
  var mc = this;
  mc.mostrar = mostrar;
  mc.prueba = "coco";

  function mostrar( tipo ) {
    var t = tipo ? "prueba-tema.html" : "prueba.html";
    ModalService.showModal( {
      templateUrl: "documentacion/views/modals/" + t,
      controller: "ModalPruebaCtrl",
      controllerAs: "prueba"
    } ).then( function( modal ) {
      modal.closed.then( function( result ) {
        mc.resultado = result ? "You said Yes" : "You said No";
      } );
    } );

  }
}
