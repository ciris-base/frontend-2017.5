"use strict";

module.exports = docNotificacionesCtrl;

function docNotificacionesCtrl( toastr ) {
  var dc = this;
  dc.mostrar = mostrar;

  function mostrar( tipo ) {
    toastr[tipo]( "Esto es una notificación", tipo );
  }
}
