"use strict";

module.exports = modalPruebaCtrl;

function modalPruebaCtrl( close )  {
  var prueba = this;
  prueba.cerrar = cerrar;

  function cerrar( resultado ) {
    return close( resultado );
  }
}
