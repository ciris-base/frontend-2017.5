"use strict";

module.exports = rutas;

function rutas( $stateProvider ) {

  $stateProvider.state( "index.doc", {
    templateUrl: "documentacion/views/index.html",
    url: "/documentacion",
    data: {
      titulo: "Documentación",
      icono: "fa-code"
    }
  } );

  $stateProvider.state( "index.doc.colores", {
    templateUrl: "documentacion/views/colores.html",
    url: "/colores",
    data: {
      titulo: "Colores",
      icono: "fa-eyedropper"
    }
  } );

  $stateProvider.state( "index.doc.tipografia", {
    templateUrl: "documentacion/views/tipografia.html",
    url: "/tipografia",
    data: {
      titulo: "Tipografía",
      icono: "fa-bold"
    }
  } );

  $stateProvider.state( "index.doc.panels", {
    templateUrl: "documentacion/views/panels.html",
    url: "/panels",
    controller: "PanelCtrl",
    controllerAs: "panel",
    data: {
      titulo: "Panels",
      icono: "fa-square"
    }
  } );

  $stateProvider.state( "index.doc.panels.tab1", {
    templateUrl: "documentacion/views/tab1.html",
    url: "/tab1",
    data: {
      titulo: "Panels",
      icono: "fa-square",
      oculto: true
    }
  } );

  $stateProvider.state( "index.doc.panels.tab2", {
    templateUrl: "documentacion/views/tab2.html",
    url: "/tab2",
    data: {
      titulo: "Panels",
      icono: "fa-square",
      oculto: true
    }
  } );

  $stateProvider.state( "index.doc.panels.tab3", {
    templateUrl: "documentacion/views/tab3.html",
    url: "/tab3",
    data: {
      titulo: "Panels",
      icono: "fa-square",
      oculto: true
    }
  } );

  $stateProvider.state( "index.doc.panels.tab4", {
    templateUrl: "documentacion/views/tab4.html",
    url: "/tab4",
    data: {
      titulo: "Panels",
      icono: "fa-square",
      oculto: true
    }
  } );

  $stateProvider.state( "index.doc.botones", {
    templateUrl: "documentacion/views/botones.html",
    url: "/botones",
    data: {
      titulo: "Botones",
      icono: "fa-battery-full"
    }
  } );

  $stateProvider.state( "index.doc.checks", {
    templateUrl: "documentacion/views/checks.html",
    url: "/checks",
    data: {
      titulo: "Checks y Radios",
      icono: "fa-dot-circle-o"
    }
  } );

  $stateProvider.state( "index.doc.forms", {
    templateUrl: "documentacion/views/forms.html",
    url: "/forms",
    data: {
      titulo: "Forms",
      icono: "fa-th-list"
    }
  } );

  $stateProvider.state( "index.doc.tablas", {
    templateUrl: "documentacion/views/tablas.html",
    url: "/tablas",
    data: {
      titulo: "Tablas",
      icono: "fa-table"
    }
  } );

  $stateProvider.state( "index.doc.notificaciones", {
    templateUrl: "documentacion/views/notificaciones.html",
    url: "/notificaciones",
    controller: "DocNotificacionesCtrl",
    controllerAs: "dc",
    data: {
      titulo: "Notificaciones",
      icono: "fa-bullhorn"
    }
  } );

  $stateProvider.state( "index.doc.modals", {
    templateUrl: "documentacion/views/modals.html",
    url: "/modals",
    controller: "DocModalsCtrl",
    controllerAs: "mc",
    data: {
      titulo: "Modals",
      icono: "fa-tablet"
    }
  } );

  $stateProvider.state( "index.doc.tests", {
    templateUrl: "documentacion/views/tests.html",
    url: "/tests",
    data: {
      titulo: "Test - Tape",
      icono: "fa-bug"
    }
  } );

  $stateProvider.state( "index.doc.tema", {
    templateUrl: "documentacion/views/tema.html",
    url: "/tema",
    data: {
      titulo: "Tema",
      icono: "fa-wrench"
    }
  } );

}
