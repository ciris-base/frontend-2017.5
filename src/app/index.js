"use strict";

var angular = require( "angular" );
var packageJson = require( "../../package.json" );

require( "angular-modal-service" );
require( "angular-hotkeys" );
require( "angular-filter" );


var modulo = angular.module( packageJson.name,
  [
  require( "angular-ui-router" ),
  require( "angular-animate" ),
  require( "angular-touch" ),
  require( "angular-toastr" ),
  require( "angular-loading-bar" ),
  require( "angular-ui-mask" ),
  require( "satellizer" ),
  "angular.filter",
  "angularModalService",
  "cfp.hotkeys",
  "Constantes",
  require( "./login" ),
  require( "./documentacion" ),
  require( "./main" ),
  require( "./admin" )
  ]
);

modulo.config( require( "./config" ) );
modulo.run( require( "./config/run.js" ) );
require( "./config/bootstrap.js" )( modulo.name );
//require( "tableexport" );

return modulo;
