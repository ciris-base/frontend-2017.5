"use strict";

var angular = require( "angular" );
var packageJson = require( "../../../package.json" );

var mod = angular.module( packageJson.name + ".Login", [] );
mod.config( require( "./rutas" ) );

mod.controller( "LoginCtrl", require( "./ctrls/loginCtrl.js" ) );

module.exports = mod.name;
