"use strict";

var _ = require( "lodash" );

module.exports = Usuario;

var modelo = {};

function Usuario( json ) {
  var temp = Object.create( modelo );
  var obj = _.assign( temp, json );
  return obj;
}
