"use strict";

module.exports = cisValidate;

function cisValidate( $compile ) {
  return {
    restrict: "A",
    require: "ngModel",
    scope: true,
    link: link
  };

  function link( scope, elem, attrs, ctrl ) {
    var html = "<div class='form__error-message'>" + attrs.mensaje + "</div>";
    var elemento = $compile( html )( scope );
    scope.$watch( function() {
      return ctrl.$viewValue;
    }, function( val ) {
      if ( val ) {
        var cond = scope.$eval( attrs.cisValidate );
        if ( cond ) {
          elem.after( elemento );
        } else {
          elemento.remove();
        }
        ctrl.$setValidity( elem[0].name, !cond );
      }
    } );
  }
}
