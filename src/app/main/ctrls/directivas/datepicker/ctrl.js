"use strict";

var modelo = require( "./modelo.js" );
var moment = require( "moment" );

module.exports = datePickerCtrl;

function datePickerCtrl( $scope, $timeout, hotkeys ) {
  var dpik = this;
  hotkeys.add( cerrar() );
  dpik.modelo = modelo( {ahora: moment()} );
  dpik.actualizar = actualizar;

  var unwatch = $scope.$watch( function() {
    return $scope.ngModelCtrl.$viewValue;
  }, function( val ) {
    if ( val ) {
      dpik.seleccion = moment( val ).format( "DD/MM/YYYY" );
      unwatch();
    }
  } );

  function actualizar( modelo ) {
    unwatch();
    dpik.seleccion = modelo;
    dpik.mostrar = false;
    $scope.ngModelCtrl.$setViewValue( modelo );
  }

  function formatter( fecha ) {
    if ( fecha ) {
      return moment( fecha, dpik.modelo.formato );
    }
  }
  $timeout( function() {
    $scope.ngModelCtrl.$parsers.push( formatter );
  }, 5 );

  function cerrarMenu() {
    dpik.mostrar = false;
  }

  function cerrar() {
    return {
      combo: "escape",
      description: "Cerrar el menú",
      callback: cerrarMenu
    };
  }
}
