"use strict";

module.exports = datePicker;

function datePicker() {
  return {
    restrict: "E",
    require: "ngModel",
    replace: true,
    scope: {
      ngDisabled: "=?"
    },
    link: link,
    controller: "DatePickerCtrl",
    controllerAs: "dpik",
    templateUrl: "main/ctrls/directivas/datepicker/template.html"
  };
}

function link( $scope, $elem, $attr, ngModelCtrl ) {
  $scope.ngModelCtrl = ngModelCtrl;
  $scope.label = $attr.label;
}
