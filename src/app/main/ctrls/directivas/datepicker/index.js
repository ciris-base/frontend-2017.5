"use strict";

var angular = require( "angular" );

var mod = angular.module( "datePickerModule", [] );

//Ctrls
mod.controller( "DatePickerCtrl", require( "./ctrl.js" ) );
mod.directive( "cisDatepicker", require( "./directive.js" ) );

module.exports = mod.name;
