"use strict";

var moment = require( "moment" );
var _ = require( "lodash" );
var formato = "DD/MM/YYYY";
require( "moment/locale/es" );
moment.locale( "es" );

module.exports = agregarLogica;

function agregarLogica( json ) {
  var modelo = {
    formato: formato,
    calcularCalendario: calcularCalendario,
    seleccionar: seleccionar,
    avanzar: avanzar,
    retroceder: retroceder,
    mesActual: mesActual
  };
  var temp = Object.create( modelo );
  var objeto = _.assign( temp, json );
  objeto.ahora = objeto.ahora || moment();
  objeto.dias = objeto.calcularCalendario( objeto.ahora );
  return objeto;
}

function seleccionar( dia ) {
  var obj = this;
  var clon = _.clone( obj );
  var sel = moment( dia, formato );
  if ( sel.isValid() ) {
    clon.ahora = sel;
    clon.dias = clon.calcularCalendario( clon.ahora, sel );
    return clon;
  } else {
    clon.dias = clon.calcularCalendario( clon.ahora );
    return clon;
  }
}

function calcularCalendario( ahora, seleccion ) {
  var d = [];
  var inicioMes = moment( ahora ).startOf( "month" );
  var actual = encontrarPrimerDomingo( inicioMes );
  var ultimo = encontrarUltimoSabado( inicioMes );
  while ( actual.isSameOrBefore( ultimo, "day" ) ) {
    d.push( crearDia( actual, ahora, seleccion ) );
    actual.add( 1, "d" );
  }
  return d;
}

function avanzar() {
  var obj = this;
  return mover( 1, obj );
}

function retroceder() {
  var obj = this;
  return mover( -1, obj );
}

function mover( valor, obj ) {
  var clon = _.clone( obj );
  clon.ahora = moment( clon.ahora ).add( valor, "month" );
  clon.dias = clon.calcularCalendario( clon.ahora );
  return clon;
}

function mesActual() {
  return this.ahora.format( "MMMM YYYY" );
}

function encontrarPrimerDomingo( fecha ) {
  var actual = moment( fecha ).day( 0 );
  return actual;
}

function encontrarUltimoSabado( fecha ) {
  var actual = moment( fecha )
  .endOf( "month" )
  .day( 6 );
  return actual;
}

function crearDia( fecha, mesActual, seleccion ) {
  var actual = moment( fecha );
  return {
    dia: actual.date(),
    fecha: actual.format( formato ),
    clases: clases().join( " " )
  };

  function clases() {
    var temp = [];
    if ( fecha.isBefore( mesActual, "month" ) || fecha.isAfter( mesActual, "month" ) ) {
      temp.push( "datepicker-cal__dia--otro" );
    } else {
      if ( fecha.isSame( moment(), "day" ) ) {
        temp.push( "datepicker-cal__dia--hoy" );
      }
    }
    if ( seleccion && fecha.isSame( seleccion, "day" ) ) {
      temp.push( "datepicker-cal__dia--active" );
    }
    return temp;
  }
}
