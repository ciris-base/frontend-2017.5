"use strict";

var test = require( "tape" );
var moment = require( "moment" );
var modelar = require( "../../modelo.js" );
var _ = require( "lodash" );

test( "Datepicker: Cálculo de días del modelo - primer domingo", calc1 );
test( "Datepicker: Cálculo de días del modelo - último sábado", calc2 );
test( "Datepicker: avanzar el calendario", avanzar1 );
test( "Datepicker: avanzar el calendario (sigue manteniendo la lógica)", avanzar2 );
test( "Datepicker: seleccionar 1 día del mismo mes", seleccionar1 );
test( "Datepicker: seleccionar 1 día de otro mes", seleccionar2 );


function calc1( assert ) {
  var modelo = modelar( {ahora: moment( "2016-10-20", "YYYY-MM-DD" )} );
  var esperado = {clases: "datepicker-cal__dia--otro", dia: 25, fecha: "25/09/2016"};
  assert.deepEquals( _.head( modelo.dias ), esperado );
  assert.end();
}

function calc2( assert ) {
  var modelo = modelar( {ahora: moment( "2016-10-20", "YYYY-MM-DD" )} );
  var esperado = {clases: "datepicker-cal__dia--otro", dia: 5, fecha: "05/11/2016"};
  assert.deepEquals( _.last( modelo.dias ), esperado );
  assert.end();
}

function avanzar1( assert ) {
  var modelo = modelar( {ahora: moment( "2016-10-20", "YYYY-MM-DD" )} );
  modelo = modelo.avanzar();
  var esperado = {clases: "datepicker-cal__dia--otro", dia: 30, fecha: "30/10/2016"};
  assert.deepEquals( _.first( modelo.dias ), esperado );
  assert.end();
}

function avanzar2( assert ) {
  var modelo = modelar( {ahora: moment( "2016-10-20", "YYYY-MM-DD" )} );
  modelo = modelo.avanzar();
  assert.equals( modelo.mesActual(), "noviembre 2016" );
  assert.end();
}

function seleccionar1( assert ) {
  var modelo = modelar( {ahora: moment( "2016-10-20", "YYYY-MM-DD" )} );
  modelo = modelo.seleccionar( "20/10/2016" );
  var esperado = {clases: "datepicker-cal__dia--otro", dia: 25, fecha: "25/09/2016"};
  assert.deepEquals( _.first( modelo.dias ), esperado );
  var dia = _.find( modelo.dias, function( dia ) {
    return dia.clases === "datepicker-cal__dia--active";
  } );
  assert.deepEquals( dia, {clases: "datepicker-cal__dia--active", dia: 20, fecha: "20/10/2016"} );
  assert.end();
}

function seleccionar2( assert ) {
  var modelo = modelar( {ahora: moment( "2016-10-20", "YYYY-MM-DD" )} );
  modelo = modelo.seleccionar( "20/11/2016" );
  var esperado = {clases: "datepicker-cal__dia--otro", dia: 30, fecha: "30/10/2016"};
  assert.deepEquals( _.first( modelo.dias ), esperado );
  var dia = _.find( modelo.dias, function( dia ) {
    return dia.clases === "datepicker-cal__dia--active";
  } );
  assert.deepEquals( dia, {clases: "datepicker-cal__dia--active", dia: 20, fecha: "20/11/2016"} );
  assert.end();
}
