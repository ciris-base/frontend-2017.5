"use strict";

module.exports = highlight;

var angular = require( "angular" );

function highlight( $sce ) {
  return function( item, search ) {
    angular.forEach( item, function( input ) {
      if ( search ) {
        var words = "(" + search.split( /\ / ).join( " |" ) + "|" + search.split( /\ / ).join( "|" ) + ")";
        var exp = new RegExp( words, "gi" );
        var normalInput = input.name.slice( search.length );
        var highlightedInput = null;
        if ( words.length ) {
          highlightedInput = input.name.slice( 0, search.length ).replace( exp, "<span class=\"ng-typeahead-highlight\">$1</span>" );
        }
        input.html = $sce.trustAsHtml( highlightedInput + normalInput );
        return;
      }
    } );
    return item;
  };
}
