"use strict";

var angular = require( "angular" );

var mod = angular.module( "odooTypeaheadModule", [] );

mod.directive( "odooTypeahead", require( "./odooTypeahead.js" ) );

mod.filter( "highlight", require( "./highlight.js" ) );

module.exports = mod.name;
