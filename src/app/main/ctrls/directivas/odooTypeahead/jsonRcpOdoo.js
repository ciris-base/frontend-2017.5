/* eslint-disable */
"use strict";
module.exports = JsonRpcOdoo;
var $ = require( "jquery" );
var _ = require( "lodash" );

function JsonRpcOdoo() {
	var temp = Object.create( {} );
	var openerp = _.assign( temp, { } );

	function genericJsonRpc( fctName, params, fct ) {
		var data = {
			jsonrpc: "2.0",
			method: fctName,
			params: params,
			id: Math.floor( Math.random() * 1000 * 1000 * 1000 )
		};
		var xhr = fct( data );
		var result = xhr.pipe( function( result ) {
			if ( typeof result.error !== "undefined" ) {
				console.error( "Server application error", result.error );
				return $.Deferred().reject( "server", result.error );
			} else {
				return result.result;
			}
		}, function() {

			//Console.error("JsonRPC communication error", _.toArray(arguments));
			var def = $.Deferred();
			return def.reject.apply( def, [ "communication" ].concat( _.toArray( arguments ) ) );
		} );

		// FIXME: jsonp?
		result.abort = function() { xhr.abort && xhr.abort(); };
		return result;
	};

	function jsonRpc( url, fctName, params, settings ) {
		return genericJsonRpc( fctName, params, function( data ) {
			return $.ajax( url, _.extend( {}, settings, {
				url: url,
				dataType: "json",
				type: "POST",
				data: JSON.stringify( data, date_to_utc ),
				contentType: "application/json"
			} ) );
		} );
	};

	var openerp = {};

	openerp.datetime_to_str = function( obj ) {
		if ( !obj ) {
			return false;
		}
		return lpad( obj.getUTCFullYear(), 4 ) + "-" + lpad( obj.getUTCMonth() + 1, 2 ) + "-"		+
         lpad( obj.getUTCDate(), 2 ) + " " + lpad( obj.getUTCHours(), 2 ) + ":"		+
         lpad( obj.getUTCMinutes(), 2 ) + ":" + lpad( obj.getUTCSeconds(), 2 );
	};

	function date_to_utc( k, v ) {
		var value = this[ k ];
		if ( !( value instanceof Date ) ) { return v; }

		return openerp.datetime_to_str( value );
	}

	openerp.url = function( path, params ) {
		params = _.extend( params || {} );
		if ( this.override_session || ( !this.origin_server ) )
		params.session_id = this.session_id;
		var qs = $.param( params );
		if ( qs.length > 0 )
		qs = "?" + qs;
		var prefix = _.some( [ "http://", "https://", "//" ], function( el ) {
			return path.length >= el.length && path.slice( 0, el.length ) === el;
		} ) ? "" : this.prefix;
		return prefix + path + qs;
	};

	function check_session_id() {
		var local_storage = JSON.parse( window.sessionStorage.usuario );
		self.session_id = local_storage.sesionOdoo;
		return $.when();
	}

	var genericJsonRpc = function( fctName, params, fct ) {
		var data = {
			jsonrpc: "2.0",
			method: fctName,
			params: params,
			id: Math.floor( Math.random() * 1000 * 1000 * 1000 )
		};
		var xhr = fct( data );
		var result = xhr.pipe( function( result ) {
			if ( result.error !== undefined ) {
				console.error( "Server application error", result.error );
				return $.Deferred().reject( "server", result.error );
			} else {
				return result.result;
			}
		}, function() {

			//Console.error("JsonRPC communication error", _.toArray(arguments));
			var def = $.Deferred();
			return def.reject.apply( def, [ "communication" ].concat( _.toArray( arguments ) ) );
		} );

		// FIXME: jsonp?
		result.abort = function() { xhr.abort && xhr.abort(); };
		return result;
	};

	openerp.jsonpRpc = function( url, fctName, params, settings ) {
		settings = settings || {};
		return genericJsonRpc( fctName, params, function( data ) {
			var payload_str = JSON.stringify( data, date_to_utc );
			var payload_url = $.param( { r:payload_str } );
			var force2step = settings.force2step || false;
			delete settings.force2step;
			var session_id = settings.session_id || null;
			delete settings.session_id;
			if ( payload_url.length < 2000 && !force2step ) {
				return $.ajax( url, _.extend( {}, settings, {
					url: url,
					dataType: "jsonp",
					jsonp: "jsonp",
					type: "GET",
					cache: false,
					data: { r: payload_str, session_id: session_id }
				} ) );
			} else {
				var args = { session_id: session_id, id: data.id };
				var ifid = _.uniqueId( "oe_rpc_iframe" );
				var html = "<iframe src='javascript:false;' name='" + ifid + "' id='" + ifid + "' style='display:none'></iframe>";
				var $iframe = $( html );
				var nurl = "jsonp=1&" + $.param( args );
				nurl = url.indexOf( "?" ) !== -1 ? url + "&" + nurl : url + "?" + nurl;
				var $form = $( "<form>" )
				.attr( "method", "POST" )
				.attr( "target", ifid )
				.attr( "enctype", "multipart/form-data" )
				.attr( "action", nurl )
				.append( $( '<input type="hidden" name="r" />' ).attr( "value", payload_str ) )
				.hide()
				.appendTo( $( "body" ) );
				var cleanUp = function() {
					if ( $iframe ) {
						$iframe.unbind( "load" ).remove();
					}
					$form.remove();
				};
				var deferred = $.Deferred();

				// The first bind is fired up when the iframe is added to the DOM
				$iframe.bind( "load", function() {

					// The second bind is fired up when the result of the form submission is received
					$iframe.unbind( "load" ).bind( "load", function() {
						$.ajax( {
							url: url,
							dataType: "jsonp",
							jsonp: "jsonp",
							type: "GET",
							cache: false,
							data: { session_id: session_id, id: data.id }
						} ).always( function() {
							cleanUp();
						} ).done( function() {
							deferred.resolve.apply( deferred, arguments );
						} ).fail( function() {
							deferred.reject.apply( deferred, arguments );
						} );
					} );

					// Now that the iframe can receive data, we fill and submit the form
					$form.submit();
				} );

				// Append the iframe to the DOM (will trigger the first load)
				$form.after( $iframe );
				if ( settings.timeout ) {
					realSetTimeout( function() {
						deferred.reject( {} );
					}, settings.timeout );
				}
				return deferred;
			}
		} );
	};
	/**
	     * Executes an RPC call, registering the provided callbacks.
	     *
	     * Registers a default error callback if none is provided, and handles
	     * setting the correct session id and session context in the parameter
	     * objects
	     *
	     * @param {String} url RPC endpoint
	     * @param {Object} params call parameters
	     * @param {Object} options additional options for rpc call
	     * @param {Function} success_callback function to execute on RPC call success
	     * @param {Function} error_callback function to execute on RPC call failure
	     * @returns {jQuery.Deferred} jquery-provided ajax deferred
	     */
	openerp.rpc = function( url, params, options ) {
		var self = this;
		options = _.clone( options || {} );
		var shadow = options.shadow || false;
		delete options.shadow;

		return check_session_id().then( function() {

			// TODO: remove
			if ( !_.isString( url ) ) {
				_.extend( options, url );
				url = url.url;
			}

			// TODO correct handling of timeouts

			var fct = openerp.jsonpRpc;
			url = openerp.url( url, null );
			options.session_id = self.session_id || "";

			var p = fct( url, "call", params, options );
			p = p.then( function( result ) {

				return result;
			}, function( type, error, textStatus, errorThrown ) {
				if ( type === "server" ) {

					if ( error.code === 100 ) {
						self.uid = false;
					}
					return $.Deferred().reject( error, $.Event() );
				} else {

					var nerror = {
						code: -32098,
						message: "XmlHttpRequestError " + errorThrown,
						data: { type: "xhr" + textStatus, debug: error.responseText, objects: [ error, errorThrown ] }
					};
					return $.Deferred().reject( nerror, $.Event() );
				}
			} );
			return p.fail( function() { // Allow deferred user to disable rpc_error call in fail
				p.fail( function( error, event ) {
					if ( !event.isDefaultPrevented() ) {
						console.log( "error", error, event );
					}
				} );
			} );
		} );
	}
  return openerp;

}
