"use strict";
var $ = require( "jquery" );
module.exports = panelOptions;

function panelOptions( ) {
  return {
    restrict: "C",
    scope: true,
    link: link
  };

  function link( $scope, $elem ) {
    setTimeout( function () {
      var elemento = $( ".layout__actions" );
      elemento.empty();
      elemento.append( $elem[0].childNodes );
    }, 5 );
  }
}
