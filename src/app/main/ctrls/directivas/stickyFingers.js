"use strict";
var $ = require( "jquery" );
module.exports = stickyFingers;

function stickyFingers( ) {
  return {
    restrict: "AC",
    scope: {},
    link: link
  };

  function link( $scope, $elem, $attr ) {
    var sel = $( ".layout__content" );
    var offset = parseInt( $attr.offset );
    sel.on( "scroll", onscroll );
    function onscroll() {
      if ( sel.scrollTop() >= parseInt( offset ) ) {
        $elem.addClass( "sticky-fingers" );
        $elem.css( "top", $attr.offset + "px" );
      } else {
        $elem.removeClass( "sticky-fingers" );
        $elem.css( "top", "" );
      }
    }
  }
}
