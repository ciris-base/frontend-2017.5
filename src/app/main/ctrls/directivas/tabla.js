"use strict";
var $ = require( "jquery" );
module.exports = tabla;

function tabla() {
  return {
    restrict: "C",
    link: link
  };
}

function link( $scope, $elem ) {
  setTimeout( function () {
    var tabla = $( $elem );
    var headers = getHeaders( tabla );
    var tds = getTds( tabla );
    setTitulosTds( headers, tds );
  }, 5 );
}

function setTitulosTds( headers, tds ) {
  var rep = headers.length;
  tds.each( function( index ) {
    var elem  = $( this );
    var texto = headers[index % rep];
    elem.attr( "data-before", texto );
  } );
}

function getTds( tabla ) {
  return tabla
    .children( ".tabla__body" )
    .children( "tr" )
    .children( "td" );
}

function getHeaders( elem ) {
  var data = [];
  var ths = elem
    .children( ".tabla__header" )
    .children( "tr" )
    .children( "th" );
  ths.each( function() {
    data.push( $( this ).text() );
  } );
  return data;
}
