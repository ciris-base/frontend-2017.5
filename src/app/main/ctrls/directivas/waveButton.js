"use strict";

var wave = require( "node-waves" );
module.exports = waveButton;

function waveButton() {
  return {
    restrict: "E",
    link: link
  };
}

function link( $scope, $elem ) {
  wave.attach( $elem[0], [ "waves-light" ] );
  wave.init();
}
