"use strict";

module.exports = NavCtrl;
var logicaMenu = require( "../models/menu.js" );

function NavCtrl( $state, hotkeys, $scope, $auth, $window, toastr, Guardian, produccion ) {
  var nav = this;
  hotkeys.add( cerrar() );
  nav.estados = logicaMenu.estadosMenu( $state.get(), Guardian, produccion );
  nav.accionar = accionar;
  nav.menuToggle = menuToggle;
  nav.salir = salir;
  nav.perfil = perfil;
  nav.usuario = JSON.parse( $window.sessionStorage.usuario );
  nav.version = require( "../../../../package.json" ).version;
  $scope.$on( "cerrar-menu", cerrarMenu );

  function accionar( estado ) {
    if ( estado.hijos && estado.hijos.length === 0 ) {
      $state.go( estado.name );
    } else {
      if ( estado.name === nav.estirado ) {
        delete nav.estirado;
      } else {
        nav.estirado = estado.name;
      }
    }
  }

  function perfil() {
    if ( nav.estirado === "perfil" ) {
      delete nav.estirado;
    } else {
      nav.estirado = "perfil";
    }
  }

  function menuToggle() {
    nav.menu = !nav.menu;
  }
  function cerrarMenu() {
    nav.menu = false;
  }

  function cerrar() {
    return {
      combo: "escape",
      description: "Cerrar el menú",
      callback: cerrarMenu
    };
  }

  function salir() {
    $auth.logout().then( function() {
      delete $window.sessionStorage.usuario;
      delete $window.localStorage.usuario;
      toastr.info( "Desconectado con éxito", "Información" );
      $state.go( "login" );
    } );
  }
}
