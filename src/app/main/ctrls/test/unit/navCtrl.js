"use strict";

var test = require( "tape" );
var _ = require( "lodash" );
var Ctrl = require( "../../navCtrl.js" );
var noHacerEstoNunca = "";

test( "navCtrl: Función accionar - primera vez", function( assert ) {
  var ins = instanciar();
  ins.accionar( {name: "Prueba"} );
  assert.equal( ins.estirado, "Prueba" );
  assert.end();
} );

test( "navCtrl: Función accionar - segunda vez", function( assert ) {
  var ins = instanciar();
  ins.accionar( {name: "Prueba"} );
  assert.equal( ins.estirado, "Prueba" );
  ins.accionar( {name: "Prueba"} );
  assert.equal( _.isUndefined( ins.estirado ), true );
  assert.end();
} );

test( "navCtrl: Función accionar - estado con hijos vacios", function( assert ) {
  var ins = instanciar();
  ins.accionar( {name: "Prueba", hijos: []} );
  assert.equal( typeof ins.estirado, "undefined" );
  assert.end();
} );

test( "navCtrl: menu toggle", function( assert ) {
  var ins = instanciar();
  ins.menuToggle();
  assert.equal( ins.menu, true );
  ins.menuToggle();
  assert.equal( ins.menu, false );
  assert.end();
} );

test( "navCtrl: salir", function( assert ) {
  var ins = instanciar();
  ins.salir();
  assert.equal( typeof ins.menu, "undefined" );
  assert.end();
} );

test( "navCtrl: hotkeys cerrar menu", function( assert ) {
  var ins = instanciar();
  noHacerEstoNunca.callback();
  assert.equal( ins.menu, false );
  assert.end();
} );

function instanciar() {
  return new Ctrl(
    {
      get: function() {
        return [];
      },
      go: function() {
        return true;
      }
    },
    {
      add: function ( cerrar ) {
        noHacerEstoNunca = cerrar;
        return cerrar;
      }
    }, {"$on": function() {
    return true;
  }}, {logout: function() {
    return {then: function( temp ) {
      temp();
      return false;
    }};
  }}, {
    sessionStorage: {usuario: "{}"},
    localStorage: "usuario"
  }, {
    info: function() { return true; }
  }, {
    ocultarModulo: function() { return true; },
    ocultarSubmodulo: function() { return true; }
  }, true );
}
