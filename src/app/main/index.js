"use strict";

var angular = require( "angular" );
var packageJson = require( "../../../package.json" );

var mod = angular.module( packageJson.name + ".main", [
  require( "./ctrls/directivas/datepicker" ),
  require( "./ctrls/directivas/odooTypeahead" )
] );
mod.config( require( "./rutas" ) );

//Ctrls
mod.controller( "BodyCtrl", require( "./ctrls/bodyCtrl.js" ) );
mod.controller( "NavCtrl", require( "./ctrls/navCtrl.js" ) );
mod.controller( "NotFoundCtrl", require( "./ctrls/notFoundCtrl.js" ) );

//Directivas
mod.directive( "button", require( "./ctrls/directivas/waveButton.js" ) );
mod.directive( "cisTitulo", require( "./ctrls/directivas/cisTitulo.js" ) );
mod.directive( "cisSelect", require( "./ctrls/directivas/selector/selector.js" ) );
mod.directive( "cisValidate", require( "./ctrls/directivas/cisValidate.js" ) );
mod.directive( "stickyFingers", require( "./ctrls/directivas/stickyFingers.js" ) );
mod.directive( "cisEnter", require( "./ctrls/directivas/cisEnter.js" ) );
mod.directive( "tabla", require( "./ctrls/directivas/tabla.js" ) );
mod.directive( "panelOptions", require( "./ctrls/directivas/panelOptions.js" ) );

//Servicios
mod.factory( "Guardian", require( "./servicios/guardian.js" ) );

module.exports = mod.name;
