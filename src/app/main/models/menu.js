"use strict";

var _ = require( "lodash" );

module.exports = {
  rejectAbstractos: rejectAbstractos,
  rejectOcultos: rejectOcultos,
  estadosMenu: estadosMenu,
  agruparHijos: agruparHijos
};

function estadosMenu( estados, Guardian, produccion ) {
  var sinAbstractos = produccion ? rejectProduccion( rejectAbstractos( estados ) ) : rejectAbstractos( estados );
  var sinOcultos = rejectOcultos( sinAbstractos, Guardian );
  return agruparHijos( sinOcultos );
}

function rejectProduccion( estados ) {
  return _.reject( estados, function( estado ) {
    return estado.name.startsWith( "index.doc" );
  } );
}

function rejectAbstractos( estados ) {
  return _.reject( estados, {abstract: true} );
}

function rejectOcultos( estados, Guardian ) {
  return _.reject( estados, function( estado ) {
    if ( estado.data && _.isFunction( estado.data.oculto ) ) {
      return estado.data.oculto( Guardian );
    }
    return estado.data ? estado.data.oculto : false;
  } );
}

function agruparHijos( estados ) {
  function recursiva( resul, lista ) {
    if ( _.isEmpty( lista ) ) {
      return resul;
    } else {
      var head = _.assign( {}, _.head( lista ) );
      var tail =  _.tail( lista );
      head.hijos = encontrarHijos( head, tail );
      resul.push( head );
      return recursiva( resul, quitarHijos( head.hijos, tail ) );
    }
  }
  return recursiva( [], estados );
}

function encontrarHijos( estado, estados ) {
  return _.filter( estados, function( elem ) {
    return elem.name.startsWith( estado.name );
  } );
}

function quitarHijos( hijos, estados ) {
  return _.difference( estados, hijos );
}
