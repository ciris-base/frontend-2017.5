"use strict";

module.exports = rutas;
rutas.$inject = [ "$stateProvider", "$urlRouterProvider" ];

function rutas( $stateProvider, $urlRouterProvider ) {

  $stateProvider.state( "index", {
    templateUrl: "main/views/layout.html",
    abstract: true
  } );

  $stateProvider.state( "index.main", {
    templateUrl: "main/views/main.html",
    url: "/main",
    data: {
      titulo: "Panel de Control",
      icono: "fa-th-large",
      oculto: true
    }
  } );

  $stateProvider.state( "404", {
    url: "/404",
    templateUrl: "main/views/404.html",
    free: true,
    controller: "NotFoundCtrl",
    controllerAs: "nfc",
    data: {
      titulo: "404 | No encontrado",
      oculto: true
    }
  } );

  $stateProvider.state( "403", {
    url: "/403",
    templateUrl: "main/views/403.html",
    data: {
      titulo: "403 | Acceso denegado",
      oculto: true
    }
  } );

  $urlRouterProvider.otherwise( "/" );
}
