"use strict";

module.exports = Guardian;

var _ = require( "lodash" );

function Guardian() {
  var payload = {};
  Guardian.setPermisos = setPermisos;
  Guardian.dejarAbrir = dejarAbrir;
  Guardian.filtrarRegistros = filtrarRegistros;
  Guardian.bloquear = bloquear;
  return Guardian;

  function setPermisos( $auth ) {
    payload = $auth.getPayload();
  }

  function dejarAbrir( params, modulo, submodulo ) {
    var permisos = payload.permisos[modulo][submodulo];
    if ( typeof permisos !== "undefined" ) {
      if ( !params.id && permisos.crear ) {
        return true;
      }
      if ( params.id && params.editar === "true" && permisos.editar ) {
        return true;
      }
      if ( params.id && ( !params.editar || params.editar === "false" ) && permisos.ver ) {
        return true;
      }
    }
    return false;
  }

  function filtrarRegistros( modulo, submodulo, registros ) {
    var ruta = modulo + ( submodulo === "" ? "" : "." + submodulo );
    var permisos = _.get( payload.permisos, ruta );
    var filtro = [];
    if ( typeof permisos !== "undefined" ) {
      _.forEach( registros, function( reg ) {
        var submodulo = permisos[reg.paso];
        if ( submodulo && submodulo.ver ) {
          filtro.push( reg );
        }
      } );
    }
    return filtro;
  }

  function bloquear( ruta ) {
    var permisos = _.get( payload, "permisos." + ruta );
    return todosFalse( permisos );
  }

  function todosFalse( permisos ) {
    return _.every( _.values( permisos ), function( permiso ) {
      if ( _.isObject( permiso ) ) {
        return todosFalse( permiso );
      } else {
        return !permiso;
      }
    } );
  }
}
