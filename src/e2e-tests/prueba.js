"use strict";

module.exports = {
  "Se loguea correctamente": loginTest,
  "El foco del input de login es el nombre": inputFocusLogin,
};

function loginTest( client ) {
  client
  .url( "http://coprimex.ciriscr.com/" )
  .waitForElementVisible( ".login__body", 1000 )
  .assert.title( "Módulo Login" )
  .setValue( "#usuario", "desarrollador@ciriscr.com" )
  .setValue( "#password", "desarrollador" )
  .click( ".login__boton" )
  .pause( 2500 )
  .assert.visible( ".boton--naranja" )
  .end();
}

function inputFocusLogin( client ) {
  client
  .url( "http://coprimex.ciriscr.com/" )
  .waitForElementVisible( ".login__body", 1000 )
  .assert.elementPresent( "#usuario:focus" )
  .end();
}
